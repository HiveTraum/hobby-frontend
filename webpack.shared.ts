import {extensions} from "./webpack/webpack";
import output from "./webpack/webpack.output";
import rules from "./webpack/webpack.rules";
import * as webpack from "webpack";

const sharedWebpackConfig: webpack.Configuration = {
    output: output,
    devtool: "eval",
    module: {
        rules: rules
    },
    resolve: {
        extensions: extensions
    }
};

export default sharedWebpackConfig;