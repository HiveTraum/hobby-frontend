import Lot from "../../../models/Lot";
import * as React from "react";
import {RouteComponentProps} from "react-router";
import services from "../../../services";
import {NonIdealState, Spinner} from "@blueprintjs/core";
import TagList from "../../Tags/TagList";
import BlueprintMarkdown from "../../../components/organisms/markdown/BlueprintMarkdown";
import BetsList from "./BetsList";

interface MatchProps {
    id: string
}

interface State {
    lot?: Lot | null,
    id: number
}

export default class LotDetail extends React.Component<RouteComponentProps<MatchProps>, State> {

    constructor(props: RouteComponentProps<MatchProps>) {
        super(props);
        this.state = {
            id: Number(props.match.params.id)
        }
    }

    componentDidMount() {
        this.fetchLot();
    }

    fetchLot = async () => {
        if (isNaN(this.state.id)) return;
        this.setState({
            lot: await services.lots.detail(this.state.id)
        })
    };

    render() {

        const {id, lot} = this.state;

        if (isNaN(id)) return <div style={{height: 400}}>
            <NonIdealState icon="error" title="Некорректный идентификатор"
                           description="Идентификатор должен состоять из числового значения"/>
        </div>;
        if (lot === undefined) return <div style={{height: 400}} className="absolute-centered-child">
            <Spinner size={50}/>
        </div>;
        if (lot === null) return <div style={{height: 400}}>
            <NonIdealState icon="zoom-out" title="404"
                           description="Не удалось найти лота с таким идентификатором"/>
        </div>;

        return <div className="container">
            <h1>{lot.title}</h1>
            <div className="mt-3"><TagList tags={lot.tags} large/></div>
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <BlueprintMarkdown className="mt-3" source={lot.technical_task}/>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <h1 className="mt-3 bp3-heading">Ставки</h1>
                    <BetsList lot={lot}/>
                </div>
            </div>
        </div>
    }
}