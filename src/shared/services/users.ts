import getEndpoint, {Endpoint} from "../layers/api/api";
import ContentType from "../enums/ContentType";
import axios from 'axios';

interface RegisterUserParams {
    email: string,
    password: string,
    first_name: string,
    last_name: string
}

export const register = (params: RegisterUserParams) => axios.post(getEndpoint(Endpoint.Users()), {
    headers: {'Content-Type': ContentType.Json},
    body: JSON.stringify(params)
});