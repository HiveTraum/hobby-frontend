import {Plugin} from "webpack";
// import * as ExtractTextPlugin from "extract-text-webpack-plugin";
// import {} from '';
import MiniCssExtractPlugin = require("mini-css-extract-plugin");

const plugins: Plugin[] = [
    new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: "styles.css",
        chunkFilename: "[id].css"
    })
];

export default plugins