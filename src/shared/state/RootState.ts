// @ts-ignore
import Cookies from 'universal-cookie';
import {AuthJWTPayload, default as JWTToken} from "../models/JWTToken";
import Cookie from "../enums/Cookie";
import Authentication from "./substates/Authentication";
import {RouterState} from "connected-react-router";
import Tag from "../models/Tag";
import IgnoredTag from "../models/IgnoredTag";

type RootState = Readonly<{
    tags: Tag[],
    ignoredTags: IgnoredTag[],
    authentication: Authentication,
    isDarkTheme: boolean,
    router?: RouterState
}>;

export default RootState;

interface InitialStateParams {
    cookies: Cookies,
    tags: Tag[]
}

export const initialState = ({cookies, tags}: InitialStateParams): RootState => {
    const authPayloadToken: string = cookies.get(Cookie.AuthToken);
    const darkTheme = cookies.get(Cookie.DarkTheme) === "true";

    return {
        tags,
        ignoredTags: [],
        isDarkTheme: darkTheme,
        authentication: {
            inAuthenticatingProcess: false,
            user: authPayloadToken ? new JWTToken<AuthJWTPayload>(authPayloadToken).payload : undefined
        }
    }
};