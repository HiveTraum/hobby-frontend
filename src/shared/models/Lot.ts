import Tag from "./Tag";

interface Author {
    id: number
    first_name: string
    last_name: string
    username: string
}

export enum Status {
    Draft = "draft",
    Canceled = "canceled",
    PlayedOut = "played_out",
    InProgress = "in_progress",
    InVerification = "in_verification",
    Accepted = "accepted"
}


export default interface Lot {
    id: number
    title: string
    technical_task: string
    author: Author
    deadline: string | null
    status: Status
    bid_winner: null | number
    tags: Tag[]
}