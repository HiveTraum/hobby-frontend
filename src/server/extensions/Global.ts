declare module NodeJS {
    interface Global {
        env: any
        atob: (str: string) => string;
        btoa: (str: string) => string;
        raven: any;
    }
}