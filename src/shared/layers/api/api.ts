import {env} from "../environment";

const URL = typeof window !== 'undefined' && window.URL ? window.URL : (global as any).URL;

export const Endpoint = {
    Users: () => "api/v1/users/",

    TokenObtain: () => "auth/v1/jwt/obtain/",
    TokenRefresh: () => "auth/v1/jwt/refresh",

    Lots: () => "api/v1/lots/",
    LotDetail: (id: number) => `api/v1/lots/${id}/`,

    Tags: () => "api/v1/tags/",

    Bets: () => "api/v1/bets/",
    BetDetail: (id: number) => `api/v1/bets/${id}`,

    IgnoredTags: () => "api/v1/ignoredtags/",
    IgnoredTagDetail: (id: number) => `api/v1/ignoredtags/${id}/`
};

const getEndpoint = (endpoint: string, query?: object): string => {
    const host = env().API;
    const url = new URL(endpoint, host);
    if (query) url.search = new URLSearchParams(Object.entries(query));
    return url.toString();
};

export interface PaginatedResult<T> {
    count: number,
    next: null | number,
    previous: null | number,
    results: T[]
}

export default getEndpoint;