import {AuthJWTPayload} from "../../models/JWTToken";

export default interface Authentication {
    user?: AuthJWTPayload & { token: string },
    error?: Error,
    inAuthenticatingProcess: boolean
}