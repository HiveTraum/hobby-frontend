import * as React from "react";
import {BLOCKQUOTE} from "@blueprintjs/core/lib/cjs/common/classes";

interface Props {
    children: string;
}

const BlockQuote = ({children}: Props) => <blockquote className={BLOCKQUOTE}>{children}</blockquote>;

export default BlockQuote;