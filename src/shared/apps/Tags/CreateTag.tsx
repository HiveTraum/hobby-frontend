import * as React from "react";
import {FormEvent, HTMLAttributes, KeyboardEvent} from "react";
import {Button, ControlGroup, InputGroup, Intent, Popover, PopoverInteractionKind} from "@blueprintjs/core";
import {ColorResult, SketchPicker} from "react-color";
import {connect} from "react-redux";
import RootState from "../../state/RootState";
import {createTag} from "../../state/actions/tags";
import Action from "../../state/actions";

interface State {
    isOpen: boolean,
    title: string,
    color: string
}

interface DispatchProps {
    createTag: (params: CreateTagParams) => void
}

interface CreateTagParams {
    title: string,
    color: string
}

class CreateTagComponent extends React.Component<HTMLAttributes<any> & DispatchProps, State> {

    state: State = {
        title: "",
        color: "#eee",
        isOpen: false
    };

    handleTitleChange = (event: FormEvent<HTMLInputElement>) => this.setState({title: event.currentTarget.value});
    handleColorChange = (color: ColorResult) => this.setState({color: color.hex});
    onSubmit = () => {
        this.props.createTag(this.state);
        this.setState({isOpen: false});
    };
    onKeyDown = (event: KeyboardEvent<HTMLInputElement>) => {
        if (event.key !== "Enter") return;
        this.onSubmit();
    };

    render() {

        const {title, color} = this.state;
        const {createTag, ...props} = this.props;

        return <Popover interactionKind={PopoverInteractionKind.CLICK}>
            <Button intent={Intent.SUCCESS} minimal icon="plus" {...props}/>
            <div>
                <SketchPicker color={color} onChange={this.handleColorChange}/>
                <ControlGroup fill>
                    <InputGroup placeholder="Название..." min={2} onChange={this.handleTitleChange} value={title}
                                onKeyDown={this.onKeyDown}/>
                    <Button icon="plus" minimal onClick={this.onSubmit} type="submit"/>
                </ControlGroup>
            </div>
        </Popover>;
    }
}

const CreateTag = connect<{}, DispatchProps, {}, RootState>(
    undefined,
    (dispatch: Action.DelayedDispatch) => ({
        createTag: (params: CreateTagParams) => dispatch(createTag(params))
    })
)(CreateTagComponent);

export default CreateTag;