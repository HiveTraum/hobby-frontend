export default interface Environment {
    API: string
    SENTRY_SERVER: string
    SENTRY_CLIENT: string
}

export const isServer = (): boolean => typeof window === "undefined";

// @ts-ignore
export const env = (): Environment => !isServer() ? window.env : global.env;