const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

import * as config from './config';
import * as webpack from 'webpack';
import plugins from "./webpack/webpack.plugins";
import sharedWebpackConfig from "./webpack.shared";

const webpackConfig: webpack.Configuration = {
    ...sharedWebpackConfig,
    entry: config.clientEntry,
    devtool: "source-map",
    mode: "production",
    plugins: plugins,
    stats: "verbose"
};

export default webpackConfig;