import * as ReactMarkdown from 'react-markdown';
import {ReactMarkdownProps} from 'react-markdown';
import * as React from 'react';
import Code from "./blocks/Code";
import BlockQuote from "./blocks/BlockQuote";
import Table from "./blocks/Table";
import Heading from "./blocks/Heading";

const r = {
    code: Code,
    blockquote: BlockQuote,
    table: Table,
    heading: Heading
};

const BlueprintMarkdown = ({renderers = r, ...props}: ReactMarkdownProps) => <ReactMarkdown {...props}
                                                                                            escapeHtml={false}
                                                                                            renderers={renderers}/>;

export default BlueprintMarkdown;