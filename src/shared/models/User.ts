import Model from "./Model";
import Service from "./Service";

export default interface User extends Model {

    username: string;
    first_name: string;
    last_name: string;
    last_login?: string;
    events: Event[];
    services: Service[];

}
