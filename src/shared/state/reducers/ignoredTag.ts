import Action from "../actions";
import IgnoredTag from "../../models/IgnoredTag";

const ignoredTags = (state: IgnoredTag[] = [], action: Action.List): IgnoredTag[] => {
    switch (action.type) {
        case Action.Type.FetchIgnoredTagsSuccess:
            return action.tags;
        case Action.Type.IgnoreTagSuccess:
            return [...state, action.tag];
        case Action.Type.RemoveIgnoredTagSuccess:
            return state.filter(tag => action.tag.id !== tag.id);
        default:
            return state;
    }
};

export default ignoredTags;