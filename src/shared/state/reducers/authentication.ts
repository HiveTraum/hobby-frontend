import Authentication from "../substates/Authentication";
import Action from "../actions";

function authentication(state: Authentication = {inAuthenticatingProcess: false}, action: Action.List): Authentication {
    switch (action.type) {
        case Action.Type.Login:
            return {inAuthenticatingProcess: true};
        case Action.Type.LoginSuccess:
            return {inAuthenticatingProcess: false, user: action.authenticatedUser};
        case Action.Type.Logout:
            return {inAuthenticatingProcess: false};
        default:
            return state;
    }
}

export default authentication;