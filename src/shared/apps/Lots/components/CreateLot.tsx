import * as React from "react";
import {EditableText} from "@blueprintjs/core";
import BlueprintMarkdown from "../../../components/organisms/markdown/BlueprintMarkdown";


interface State {
    title: string,
    text: string
}

export default class CreateLot extends React.Component<{}, State> {

    state: State = {
        title: "",
        text: ""
    };

    handleTitle = (value: string) => this.setState({title: value});
    handleText = (value: string) => this.setState({text: value});

    render() {
        return <div className="container">
            <h1>
                <EditableText maxLength={150} placeholder="Заголовок..." value={this.state.title} multiline
                              onChange={this.handleTitle}/>
            </h1>
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 mt-3">
                    <EditableText multiline placeholder="Техническое задание..." value={this.state.text}
                                  onChange={this.handleText} minLines={5}/>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 mt-3">
                    <BlueprintMarkdown source={this.state.text}/>
                </div>
            </div>
        </div>
    }
}