enum Cookie {
    AuthToken = 'auth-token',
    DarkTheme = 'dark-theme'
}

export default Cookie;