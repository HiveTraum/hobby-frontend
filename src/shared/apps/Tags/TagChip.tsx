import * as Color from "color";
import {ITagProps, Tag as TagComponent} from "@blueprintjs/core";
import * as React from "react";
import Tag from "../../models/Tag";
import {connect} from "react-redux";
import RootState from "../../state/RootState";

interface TagChipProps {
    tag: Tag,
    onTagClick?: (tag: Tag) => void,
    dispatch?: Function,
    inProcess?: boolean
}

interface PropsFromState {
    isDarkTheme: boolean
}

type OwnProps = TagChipProps & ITagProps;

const isWhite = (isDarkTheme: boolean, isDarkBackground: boolean, minimal: boolean) =>
    (isDarkBackground && !isDarkTheme && !minimal) ||
    (isDarkBackground && isDarkTheme && !minimal) ||
    (!isDarkBackground && isDarkTheme && minimal) ||
    (isDarkBackground && isDarkTheme && minimal);

const TagChipComponent = (props: OwnProps & PropsFromState) => {

    // TODO Откуда здесь dispatch? Разобраться

    const {tag, onTagClick, isDarkTheme, dispatch: _, ...tagProps} = props;
    let color = Color(tag.color);
    const isDark = color ? color.isDark() : false;
    const textColor = isWhite(isDarkTheme, isDark, props.minimal === undefined ? false : props.minimal) ? "white" : "black";
    return <span className="mr-1">
                <TagComponent {...tagProps} style={{
                    backgroundColor: props.minimal ? undefined : color.rgb().string(),
                    color: textColor
                }} onClick={() => onTagClick ? onTagClick(tag) : undefined}>{tag.title}</TagComponent>
            </span>
};

const TagChip = connect<PropsFromState, {}, OwnProps, RootState>(
    (state, ownProps) => ({isDarkTheme: state.isDarkTheme, ...ownProps})
)(TagChipComponent);

export default TagChip;