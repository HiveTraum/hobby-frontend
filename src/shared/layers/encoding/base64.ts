import {isServer} from "../environment";

export const decode = (str: string): string => {
    if (isServer()) return global.atob(str);
    else return decodeURIComponent(
        window.atob(str)
            .split('')
            .map(c => '%' + ('00' + c
                .charCodeAt(0)
                .toString(16))
                .slice(-2))
            .join(""))
};

export const encode = (str: string): string => {
    if (isServer()) return global.btoa(str);
    else return window.btoa(encodeURIComponent(str)
        .replace(
            /%([0-9A-F]{2})/g,
            (substring: string) => String.fromCharCode(Number('0x' + substring))
        ))
};