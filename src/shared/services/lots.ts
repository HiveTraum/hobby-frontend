import getEndpoint, {Endpoint, PaginatedResult} from "../layers/api/api";
import Lot from "../models/Lot";
import axios from 'axios';


interface ListParams {
    page?: number,
    search?: string,
    ignored?: boolean
}

export const list = async (params: ListParams = {}): Promise<PaginatedResult<Lot>> => {
    const response = await axios.get(getEndpoint(Endpoint.Lots(), params));
    if (response.status !== 200) return {count: 0, next: null, previous: null, results: []};

    return response.data;
};


export const detail = async (id: number): Promise<Lot | null> => {
    const response = await axios.get(getEndpoint(Endpoint.LotDetail(id)));
    if (response.status !== 200) return null;
    return response.data;
};