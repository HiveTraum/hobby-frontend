import * as React from 'react';
import {ReactNode} from "react";

interface Props {
    active: boolean,
    children?: ReactNode,
    onClick: () => void
}

const Link = ({active, children, onClick}: Props) => {
    if (active) return <span>{children}</span>;

    return <a href="" onClick={e => {
        e.preventDefault();
        onClick();
    }}>
        {children}
    </a>;
};

export default Link;