import * as React from "react";
import Tag from "../../models/Tag";
import TagChip from "./TagChip";
import {connect} from "react-redux";
import RootState from "../../state/RootState";
import {ignoreTag, removeIgnoredTag} from "../../state/actions/ignoredTags";
import Action from "../../state/actions/index";
import IgnoredTag from "../../models/IgnoredTag";
import CreateTag from "./CreateTag";

interface OwnProps {
    onClick?: (tag: Tag) => void
}

interface Props {
    tags: Tag[],
    ignoredTags: IgnoredTag[]
}

interface DispatchProps {
    ignoreTag: (tag: Tag) => void,
    removeIgnoredTag: (tag: IgnoredTag) => void
}

class TagSelectListComponent extends React.Component<Props & OwnProps & DispatchProps> {


    onTagClick = (tag: Tag) => {
        const {ignoredTags, removeIgnoredTag, ignoreTag, onClick} = this.props;
        const ignoredTag = ignoredTags.find(ignoredTag => ignoredTag.tag === tag.id);
        ignoredTag !== undefined ? removeIgnoredTag(ignoredTag) : ignoreTag(tag);
        if (onClick) onClick(tag);
    };

    render() {
        const {tags, ignoredTags, ignoreTag} = this.props;
        const ignoredTagIdentifiers = ignoredTags.map(tag => tag.tag);
        return <div style={{display: "flex", flexWrap: "wrap"}}>
            {tags.map(tag => <TagChip className="mt-2" interactive key={tag.id} tag={tag}
                                      minimal={ignoredTagIdentifiers.includes(tag.id)}
                                      large onTagClick={this.onTagClick}/>)}

            <CreateTag className="mt-2"/>

        </div>
    }
}


const TagSelectList = connect<Props, DispatchProps, OwnProps, RootState>(
    (state, ownProps) => ({
        tags: state.tags,
        ignoredTags: state.ignoredTags,
        ...ownProps
    }),
    (dispatch: Action.DelayedDispatch) => ({
        removeIgnoredTag: (tag: IgnoredTag) => dispatch(removeIgnoredTag(tag)),
        ignoreTag: (tag: Tag) => dispatch(ignoreTag(tag))
    })
)(TagSelectListComponent);

export default TagSelectList;