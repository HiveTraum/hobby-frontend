import * as React from "react";
import {
    HTML_TABLE,
    HTML_TABLE_BORDERED,
    HTML_TABLE_STRIPED
} from "@blueprintjs/core/lib/cjs/common/classes";

interface Props {
    children: string;
}

const Table = ({children}: Props) => <table className={`${HTML_TABLE} ${HTML_TABLE_BORDERED} ${HTML_TABLE_STRIPED}`} style={{width: "100%"}}>
    {children}
</table>;

export default Table;