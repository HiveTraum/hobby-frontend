import * as React from 'react';
import {renderToString} from "react-dom/server";
import {StaticRouter} from "react-router";
import App from "../shared/App";
import {Request, Response} from "express-serve-static-core";
import RootState, {initialState} from "../shared/state/RootState";
import {createStore} from "redux";
import reducer from "../shared/state/reducers/index";
import {bundleTitle, staticPath} from '../../config';
import {Provider} from "react-redux";
import * as dotenv from "dotenv";
import Environment from "../shared/layers/environment";
// @ts-ignore
import * as Cookies from 'universal-cookie';
// @ts-ignore
import * as btoa from 'btoa';
// @ts-ignore
import * as atob from 'atob';
// @ts-ignore
import * as Raven from 'raven';
import services from "../shared/services";
import Tag from "../shared/models/Tag";


dotenv.config();

global.env = {
    API: process.env.API,
    SENTRY_SERVER: process.env.SENTRY_SERVER,
    SENTRY_CLIENT: process.env.SENTRY_CLIENT
};

global.atob = atob;
global.btoa = btoa;
global.raven = Raven;


interface Params {
    content: string,
    state: RootState,
    env: Environment
}

const indexTemplate = ({content, state, env}: Params): string => {
    return `<html lang="ru">
	<head>
		<meta charSet="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>EVENTS</title>
		<link href="${staticPath}styles.css" rel="stylesheet"/>
	</head>
	<body>
		<div id="root">${content}</div>
		<script>
			window.__PRELOADED_STATE__ = ${JSON.stringify(state).replace(/</g, '\\\u003c')};
			window.env = ${JSON.stringify(env).replace(/</g, '\\\u003c')};
		</script>
		<script async src="${staticPath}${bundleTitle}"></script>
	</body>
	</html>`
};


export const initialHandler = async (request: Request, response: Response) => {

    let tags: Tag[];

    try {
        tags = await services.tags.list();
    } catch (e) {
        console.log(e);
        tags = [];
    }

    const store = createStore(reducer, initialState({
        cookies: new Cookies(request.headers.cookie),
        tags: tags
    }));

    const content = renderToString(<Provider store={store}>
        <StaticRouter location={request.url} context={{}}>
            <App/>
        </StaticRouter>
    </Provider>);

    response.end(indexTemplate({
        content,
        state: store.getState(),
        env: global.env
    }));
};