import axios from 'axios';
import getEndpoint, {Endpoint} from "../layers/api/api";
import Bet from "../models/Bet";

interface ListParams {
    performer?: number,
    lot?: number
}

export const list = async (params: ListParams): Promise<Bet[]> => {
    const response = await axios.get(getEndpoint(Endpoint.Bets(), params));
    if (response.status === 200) return response.data;
    return [];
};

interface CreateParams {
    bid: number,
    lot: number
}

export const create = (params: CreateParams) => axios.post<CreateParams>(getEndpoint(Endpoint.Bets()), params);

export const remove = (id: number) => axios.delete(getEndpoint(Endpoint.BetDetail(id)));