enum ContentType {
    Json = "application/json"
}

export default ContentType;