import Action from "./index";
import services from "../../services";
import Tag from "../../models/Tag";

export type CreateTagAction = Action.Delayed;

interface CreateTagParams {
    title: string,
    color: string
}

export const createTag = (params: CreateTagParams): CreateTagAction => async (dispatch: Action.DelayedDispatch): Promise<void> => {
    try {
        const response = await services.tags.create(params);
        if (response.status === 201) dispatch(createTagSuccessful(response.data));
    } catch (e) {

    }
};

export type FetchTagsAction = Action.Delayed;

export const fetchTags = (): FetchTagsAction => async (dispatch: Action.DelayedDispatch): Promise<void> => {
    try {
        const response = await services.tags.list();
        dispatch(fetchTagsSuccessful(response));
    } catch (e) {

    }
};


export interface CreateTagSuccessfulAction extends Action.Reducible {
    type: Action.Type.CreateTagSuccess,
    tag: Tag
}

export const createTagSuccessful = (tag: Tag): CreateTagSuccessfulAction => ({
    type: Action.Type.CreateTagSuccess,
    tag
});

export interface FetchTagsSuccessfulAction {
    type: Action.Type.FetchTagsSuccess,
    tags: Tag[]
}

export const fetchTagsSuccessful = (tags: Tag[]): FetchTagsSuccessfulAction => ({
    type: Action.Type.FetchTagsSuccess,
    tags
});