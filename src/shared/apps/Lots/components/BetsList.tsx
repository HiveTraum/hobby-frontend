import * as React from "react";
import {Button, Card, ControlGroup, Elevation, NonIdealState, NumericInput, Spinner} from "@blueprintjs/core";
import Bet from "../../../models/Bet";
import Lot from "../../../models/Lot";
import services from "../../../services";
import {connect} from "react-redux";
import {AuthJWTPayload} from "../../../models/JWTToken";
import RootState from "../../../state/RootState";

interface StoreProps {
    user?: AuthJWTPayload
}

interface OwnProps {
    lot: Lot
}

interface State {
    bets: Bet[],
    isLoading: boolean,
    bid: number
}

const MAX_SMALL_INT = 32766;
const MIN_SMALL_INT = 1;

class BetsListComponent extends React.Component<OwnProps & StoreProps, State> {

    state: State = {
        bets: [],
        isLoading: false,
        bid: 1
    };

    componentDidMount() {
        this.setState({isLoading: true}, this.fetchBets);
    }

    fetchBets = () => services.bets.list({lot: this.props.lot.id}).then(bets => this.setState({
        bets: bets.sort((a, b) => a.bid - b.bid),
        isLoading: false
    }));

    createBet = async () => {
        const response = await services.bets.create({lot: this.props.lot.id, bid: this.state.bid});
        if (response.status === 201) this.fetchBets();
    };

    deleteBet = async (id: number) => {
        const response = await services.bets.remove(id);
        if (response.status === 204) this.fetchBets();
    };

    setBid = (bid: number) => {
        if (bid >= MAX_SMALL_INT) bid = MAX_SMALL_INT;
        if (bid <= MIN_SMALL_INT) bid = MIN_SMALL_INT;
        this.setState({bid});
    };

    render() {

        const {bets, isLoading, bid} = this.state;
        const {user} = this.props;

        if (isLoading) return <Spinner size={20}/>;

        const createBetView = <ControlGroup fill>
            <NumericInput min={MIN_SMALL_INT} value={bid} large onValueChange={this.setBid} fill max={MAX_SMALL_INT}
                          allowNumericCharactersOnly stepSize={1}/>
            <Button icon="bank-account" onClick={this.createBet}/>
        </ControlGroup>;

        if (bets.length <= 0) return <div style={{height: 200}}>
            <NonIdealState title="Ставки не найдены"
                           description="Вы можете сделать первую ставку на этот лот"
                           icon="bank-account" action={createBetView}/>
        </div>;

        const betIsMade = user ? bets.map(bet => bet.performer.id).includes(user.user_id) : false;
        const betByCurrentUser = user ? bets.find(bet => bet.performer.id === user.user_id) : undefined;

        return <div>
            {bets.map(bet => {
                const elevation = betByCurrentUser && betByCurrentUser.id === bet.id ? Elevation.FOUR : Elevation.ZERO;
                return <Card key={bet.id} style={{borderRadius: 0}} elevation={elevation}>
                    <p style={{margin: 0}}>{bet.bid} ч.
                        - {bet.performer.last_name} {bet.performer.first_name.charAt(0).toUpperCase()}.</p>
                </Card>
            })}
            {!betIsMade ? <div className="mt-3">
                {createBetView}
            </div> : betByCurrentUser ?
                <Button className="mt-3" fill minimal large onClick={() => this.deleteBet(betByCurrentUser.id)}>Удалить
                    ставку</Button> : undefined}
        </div>;
    }
}

const BetsList = connect<StoreProps, {}, OwnProps, RootState>(
    (state, ownProps) => ({
        lot: ownProps.lot,
        user: state.authentication.user
    })
)(BetsListComponent);

export default BetsList;