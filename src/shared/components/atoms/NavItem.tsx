import * as React from 'react';

interface Props {
    label: string,
    key: string,
    disabled?: boolean,
    active?: boolean,
    onClick?: (key: string) => any
}

export default (props: Props) => {

    const className = ['nav-link'];

    if (props.active) className.push('active');

    return <li className="nav-item">
        <a href="" className={className.join(" ")} onClick={evt => {
            evt.preventDefault();
            if (props.onClick) props.onClick(props.key);
        }}>{props.label}</a>
    </li>;
}