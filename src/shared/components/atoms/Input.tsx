import * as React from 'react';
import {ChangeEvent} from 'react';

export enum InputSize {
    Large = "form-control-lg",
    Small = "form-control-sm"
}

interface Props {
    placeholder?: string,
    size?: InputSize,
    name: string,
    onChange?: (field: string, value: string | null) => any,
    value?: string | null,
    errors?: string[],
    help?: string,
    label: string,
    type?: string,
    required?: boolean,
    max?: number,
    maxLength?: number,
    isSecure?: boolean
}


export default class Input extends React.Component<Props> {

    onChange = (event: ChangeEvent<HTMLInputElement>) => {
        if (this.props.onChange) this.props.onChange(this.props.name, event.target.value.length > 0 ? event.target.value : null);
    };

    render() {

        const {value, errors, help, label, name, type, required, max, maxLength, size} = this.props;

        let className = ["form-control"];
        if (size) className.push(size);
        if (errors && errors.length > 0) className.push('is-invalid');
        return <div key={name} className="form-group">
            <label>{label}</label>
            <input className={className.join(" ")} type={type && type === "string" ? "text" : type}
                   placeholder={label} name={name} maxLength={maxLength} max={max} required={required}
                   onChange={this.onChange} value={value ? value : ""}/>
            {errors ? errors.map(e => <div key={e} className="invalid-feedback">{e}</div>) : ""}
            {help ? <small className="form-text text-muted">{help}</small> : ""}
        </div>
        //
        // return <input type={this.props.type} className={className.join(" ")}
        //               placeholder={this.props.placeholder ? this.props.placeholder : ""}
        //               onChange={this.onChange} value={this.props.value !== null ? this.props.value : ""}/>
    }
}