import * as React from "react";
import {Route, RouteComponentProps, Switch, withRouter} from "react-router";
import routes from './routes';
import {hot} from 'react-hot-loader';
import NavigationBar from "./apps/NavigationBar";
import {connect} from "react-redux";
import RootState from "./state/RootState";
import axios from 'axios';
import './layers/http';
import HTTPHeader from "./enums/HTTPHeader";
import {DARK} from "@blueprintjs/core/lib/cjs/common/classes";

interface Props {
    darkTheme: boolean,
    token: string
}

const AppComponent = ({darkTheme, token}: Props & RouteComponentProps<{}>) => {

    if (token) axios.defaults.headers.common[HTTPHeader.Authorization] = `JWT ${token}`;
    else delete axios.defaults.headers.common[HTTPHeader.Authorization];

    if (typeof document !== "undefined") {
        if (darkTheme) document.body.classList.add(DARK);
        else document.body.classList.remove(DARK)
    }

    const classNames = ["lpq-root"];
    if (darkTheme) classNames.push(DARK);

    return <div className={classNames.join(" ")}>
        <NavigationBar/>
        <div className="mt-3 mb-3">
            <Switch>{routes.map(route => <Route key={route.path} {...route}/>)}</Switch>
        </div>
    </div>
};

const App = withRouter(connect(
    (state: RootState) => ({
        darkTheme: state.isDarkTheme,
        token: state.authentication.user ? state.authentication.user.token : undefined
    })
)(AppComponent));

export default hot(module)(App);
