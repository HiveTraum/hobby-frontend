export type Hashable = Object | string | number;

export interface WriteOptions {
    expire?: number
}

export default interface KeyValueStore {

    get(key: Hashable): Promise<Object | number | string | null>;

    set(key: Hashable, value: any, options: WriteOptions): Promise<boolean>;

}