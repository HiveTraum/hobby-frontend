// import * as ExtractTextPlugin from "extract-text-webpack-plugin";
import MiniCssExtractPlugin = require("mini-css-extract-plugin");

import {Loader, RuleSetRule} from "webpack";

// export const generateStyleLoaders = (...loaders: Loader[]) => {
//     return loaders.map(loader => (
//         {
//             loader,
//             options: {
//                 url: true,
//                 sourceMap: !!process.env.SOURCE_MAP,
//                 minimize: false
//             }
//         }
//     ))
// };

const rules: RuleSetRule[] = [
    // {test: /\.jsx$/, include: config.src, loader: "babel-loader"},
    {
        test: /\.(ts|tsx)?$/, use: ['babel-loader', {
            loader: "awesome-typescript-loader", options: {
                configFile: 'client.tsconfig.json'
            }
        }]
    },
    {
        test: /\.s?[ac]ss$/,
        use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            {
                loader: 'postcss-loader',
                options: {
                    plugins: () => [
                        require('autoprefixer')({browsers: ['last 3 versions', 'iOS 9']}),
                    ]
                }
            },
            'sass-loader',
        ],
    },
    {
        test: /\.(eot|woff|woff2|ttf|png|jpg|otf|gif|svg)$/,
        loader: 'file-loader?limit=30000&name=[name]-[hash].[ext]',
        options: {
            name: '[name]-[hash].[ext]'
        }
    }
];

export default rules;