import Lot from "../../../models/Lot";
import * as React from "react";
import {Card, H5} from "@blueprintjs/core";
import {RouteComponentProps, withRouter} from "react-router";
import {references} from "../../../routes";
import TagList from "../../Tags/TagList";
import {Link} from "react-router-dom";

interface Props {
    lot: Lot
}

const LotCard = ({lot}: Props & RouteComponentProps<{}>) => {
    return <Link to={references.lot(lot.id)}>
        <Card interactive>
            <H5>{lot.title}</H5>
            <TagList tags={lot.tags}/>
        </Card>
    </Link>
};

export default withRouter(LotCard);