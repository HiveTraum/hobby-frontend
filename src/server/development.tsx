import * as express from 'express';
import * as React from 'react';
import {initialHandler} from "./index";

import * as webpack from 'webpack';
import * as devConfig from '../../webpack.development';
import * as devMiddleware from 'webpack-dev-middleware';
import * as hotMiddleware from 'webpack-hot-middleware';
import getRaven from "../shared/layers/raven";

const Raven = getRaven();

const app = express();

const compiler = webpack(devConfig.default);

const publicPath = devConfig.default.output && devConfig.default.output.publicPath
    ? devConfig.default.output.publicPath
    : "/";

app.use(Raven.requestHandler());

app.get('/', initialHandler);

app.use(devMiddleware(compiler, {
    publicPath
}));

app.use(hotMiddleware(compiler));

app.get('*', initialHandler);

app.use(Raven.errorHandler());

app.use((err: any, req: any, res: any, next: any) => {
    res.status(500);
    res.end(res.sentry + '\n');
});

const PORT = 3000;

app.listen(PORT, (error: Error) => {
    console.log(`http://localhost:${PORT}`);

    if (error) return console.error(error.message);
});