import Action from "../actions";
import Tag from "../../models/Tag";

const tags = (state: Tag[] = [], action: Action.List): Tag[] => {
    switch (action.type) {
        case Action.Type.CreateTagSuccess:
            return [...state, action.tag];
        default:
            return state;
    }
};

export default tags;