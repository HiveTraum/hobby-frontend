import {Alignment, Button, Intent, Navbar, Popover, Position} from "@blueprintjs/core";
import routes from "../routes";
import {Link} from "react-router-dom";
import * as React from 'react';
import Entrance from "./Entrance/components/Entrance";
import {connect} from "react-redux";
import RootState from "../state/RootState";
import {logout} from "../state/actions/authentication";
import Authentication from "../state/substates/Authentication";
import {switchTheme} from "../state/actions/theme";

interface Props {
    auth: Authentication,
    dark: boolean
}

interface DispatchProps {
    onLogoutClick: () => void,
    onSwitchThemeClick: (dark: boolean) => void
}

const NavigationBarComponent = (props: Props & DispatchProps) => {

    const {auth: {user}, onLogoutClick, onSwitchThemeClick, dark} = props;

    const userPopoverContent = <div className="hb-popover-content">
        <Entrance/>
    </div>;

    const logoutPopover = <div className="hb-popover-content">
        <h4>Привет {user ? user.username : ""}</h4>
        <Button intent={Intent.DANGER} text="Выйти" fill large onClick={onLogoutClick}/>
    </div>;

    return <Navbar>
        <Navbar.Group align={Alignment.LEFT}>
            <Navbar.Heading><Link to="/">LEPQa</Link></Navbar.Heading>
            <Navbar.Divider/>
            {routes
                .filter(route => route.path !== '/' && route.showInNavigation)
                .map(route => <Link to={route.path || '/'} key={route.path}>
                    <Button minimal text={route.title}/>
                </Link>)}
        </Navbar.Group>

        <Navbar.Group align={Alignment.RIGHT}>
            <Popover minimal content={user ? logoutPopover : userPopoverContent}
                     position={Position.BOTTOM_RIGHT}>
                <Button minimal icon="user" text={user ? user.username : ""}/>
            </Popover>
            <Button minimal icon={dark ? "flash" : "moon"} onClick={() => onSwitchThemeClick(!dark)}/>
        </Navbar.Group>
    </Navbar>
};

const NavigationBar = connect<Props, DispatchProps, {}, RootState>(
    state => ({
        auth: state.authentication,
        dark: state.isDarkTheme
    }),
    dispatch => ({
        onLogoutClick: () => dispatch(logout()),
        onSwitchThemeClick: (dark: boolean) => dispatch(switchTheme(dark))
    })
)(NavigationBarComponent);

export default NavigationBar;