import {RouteComponentProps, RouteProps} from "react-router";
import Home from "./apps/Home";
import * as React from "react";
import LotsApp from "./apps/Lots/LotsApp";
import LotDetail from "./apps/Lots/components/LotDetail";
import CreateLot from "./apps/Lots/components/CreateLot";


class Route implements RouteProps {
    title: string;
    path: string;
    component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
    exact: boolean;
    showInNavigation: boolean;

    constructor(path: string, component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>, title: string, {exact = true, showInNavigation = false} = {}) {
        this.title = title;
        this.path = path;
        this.component = component;
        this.exact = exact;
        this.showInNavigation = showInNavigation;
    }
}

export const references = {
    index: () => "/",

    lots: () => "/lots",
    lot: (id: number) => `/lots/${id}`,
    createLot: () => "/lots/create"
};

const routes: Route[] = [
    new Route(references.index(), Home, "LEPQa"),
    new Route(references.lots(), LotsApp, "Лоты", {showInNavigation: true}),
    new Route(references.createLot(), CreateLot, "Создать Лот"),
    new Route("/lots/:id", LotDetail, "Лот")
];

export default routes;