import * as React from 'react';
import {FormEvent} from 'react';
import {Button, ControlGroup, FormGroup, InputGroup, Intent, Tooltip} from "@blueprintjs/core";
import {register} from "../../../services/users";
import {connect} from "react-redux";
import RootState from "../../../state/RootState";
import {ThunkDispatch} from "redux-thunk";
import {login} from "../../../state/actions/authentication";
import Action from "../../../state/actions";

type FormState = {
    email: string,
    password: string,
    repeatedPassword: string,
    first_name: string,
    last_name: string
}

interface LoginCredentials {
    username: string,
    password: string
}

type FieldValidator<T, P> = (value: P, form?: T) => string | undefined;

type Validator<T> = {
    [P in keyof T]: FieldValidator<T, T[P]>
}

type Errors<T> = {
    [P in keyof T]?: string
}

interface State {
    showPassword: boolean,
    errors: Errors<Partial<FormState>>,
    fields: FormState
}

interface DispatchProps {
    onLogin: (credentials: LoginCredentials) => void
}

const PASSWORDS_MUST_BE_EQUAL = "Пароли должны совпадать";

class RegistrationFormComponent extends React.Component<DispatchProps, State> {
    state: State = {
        showPassword: false,
        errors: {},
        fields: {
            email: "",
            password: "",
            repeatedPassword: "",
            first_name: "",
            last_name: ""
        }
    };

    validator: Validator<Partial<FormState>> = {
        email: value => (value && value.includes("@sdvor.com")) ? undefined : "Почта должна быть корпоративной(example@sdvor.com)"
    };

    handleLockClick = () => this.setState({showPassword: !this.state.showPassword});

    handleInput = (event: FormEvent<HTMLInputElement>) => {
        const label = event.currentTarget.name as keyof FormState;
        const value = event.currentTarget.value;

        this.setState(prevState => {
            const fields = {...prevState.fields, [label]: value};
            const validator = this.validator[label];
            const error = validator ? validator(value, fields) : undefined;
            const errors = Object
                .entries({...prevState.errors, [label]: error})
                .filter(value => Boolean(value[1]))
                .reduce((previousValue, currentValue) => ({...previousValue, [currentValue[0]]: currentValue[1]}), {});
            return {...prevState, fields, errors}
        });
    };

    onSubmit = async (event: React.FormEvent<HTMLElement>) => {
        event.preventDefault();
        try {
            const response = await register(this.state.fields);
            if (response.status === 201) {
                const body = JSON.parse(response.data);
                this.props.onLogin({username: body.username, password: this.state.fields.password});
            } else if (response.status === 400) {
                const body = JSON.parse(response.data);
                const errors = Object.entries(body).reduce((previousValue, [key, value]) => ({
                    ...previousValue,
                    // @ts-ignore
                    [key]: value[0]
                }), {});
                console.log(errors);
                this.setState(prevState => ({...prevState, errors: {...prevState.errors, ...errors}}));
            }
        } catch (e) {

        }
    };

    render() {
        const {showPassword, fields: {email, first_name, last_name, password, repeatedPassword}, errors} = this.state;

        const passwordError = password !== repeatedPassword ? PASSWORDS_MUST_BE_EQUAL : undefined || errors.password;

        const lockButton = <Tooltip content={`${showPassword ? "Скрыть" : "Показать"} пароль`}>
            <Button icon={showPassword ? "unlock" : "lock"} intent={Intent.WARNING} minimal
                    onClick={this.handleLockClick}/>
        </Tooltip>;

        return <form onSubmit={this.onSubmit}>
            <FormGroup key="first_name" label="Имя">
                <InputGroup large required type="text" value={first_name} name="first_name"
                            onChange={this.handleInput} placeholder="Ваше настоящее имя"/>
            </FormGroup>
            <FormGroup key="last_name" label="Фамилиля">
                <InputGroup large required type="text" value={last_name} name="last_name"
                            onChange={this.handleInput} placeholder="Ваша настоящяя фамилия"/>
            </FormGroup>
            <FormGroup key="email" label="Email" intent={errors.email ? Intent.DANGER : Intent.NONE}
                       helperText={errors.email ? errors.email : undefined}>
                <InputGroup large required placeholder="email@sdvor.com" value={email}
                            name="email" onChange={this.handleInput}
                            intent={errors.email ? Intent.DANGER : Intent.NONE}/>
            </FormGroup>
            <FormGroup key="repeatedPassword" label="Пароль"
                       helperText={passwordError} intent={passwordError ? Intent.DANGER : Intent.NONE}>
                <ControlGroup vertical fill>
                    <InputGroup large required type={showPassword ? "text" : "password"} rightElement={lockButton}
                                value={password} onChange={this.handleInput} name="password"
                                placeholder="Введите пароль..."
                                intent={passwordError ? Intent.DANGER : Intent.NONE}/>
                    <InputGroup large required type={showPassword ? "text" : "password"}
                                placeholder="...и повторите его!"
                                value={repeatedPassword} onChange={this.handleInput} name="repeatedPassword"
                                intent={passwordError ? Intent.DANGER : Intent.NONE}/>
                </ControlGroup>
            </FormGroup>

            <Button large fill text="Зарегистрироваться" key="register-button" type="submit"
                    disabled={Object.keys(errors).length !== 0 || passwordError !== undefined} onClick={this.onSubmit}/>
        </form>;
    }
}

const RegistrationForm = connect<{}, DispatchProps, {}, RootState>(
    state => ({inAuthenticationProcess: state.authentication.inAuthenticatingProcess}),
    (dispatch: ThunkDispatch<RootState, {}, Action.Reducible>) => ({onLogin: (credentials: LoginCredentials) => dispatch(login(credentials))})
)(RegistrationFormComponent);

export default RegistrationForm;