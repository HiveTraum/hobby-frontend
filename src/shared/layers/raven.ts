import {env, isServer} from "./environment";
import * as Raven from 'raven-js';

let installed = false;
const getRaven = () => {

    const raven = isServer() ? global.raven : Raven;

    if (!installed) {
        const dsn = isServer() ? env().SENTRY_SERVER : env().SENTRY_CLIENT;
        console.log(dsn);
        raven.config(dsn).install();
        installed = true;
    }

    return raven
};

export default getRaven;