import Action from "./index";

import services from "../../services";
import IgnoredTag from "../../models/IgnoredTag";
import Tag from "../../models/Tag";

export type FetchIgnoredTagsAction = Action.Delayed;

export const fetchIgnoredTags = (): FetchIgnoredTagsAction => async (dispatch: Action.DelayedDispatch): Promise<void> => {

    try {
        const response = await services.ignoredTags.list();
        if (response.status === 200) dispatch(fetchIgnoredTagsSuccess(response.data));
    } catch (e) {
        dispatch(fetchIgnoredTagsSuccess([]));
    }
};

export type IgnoreTagAction = Action.Delayed;

export const ignoreTag = (tag: Tag): IgnoreTagAction => async (dispatch: Action.DelayedDispatch): Promise<void> => {

    try {
        const response = await services.ignoredTags.create(tag.id);
        if (response.status === 201) dispatch(ignoreTagSuccessful(response.data));
    } catch (e) {

    }
};

export type RemoveIgnoredTagAction = Action.Delayed;

export const removeIgnoredTag = (ignoredTag: IgnoredTag): RemoveIgnoredTagAction => async (dispatch: Action.DelayedDispatch, getState): Promise<void> => {

    try {
        const response = await services.ignoredTags.remove(ignoredTag.id);
        if (response.status === 204) dispatch(removeIgnoredTagSuccessful(ignoredTag));
    } catch (e) {
        dispatch(fetchIgnoredTagsSuccess([]));
    }
};

export interface IgnoreTagSuccessfulAction extends Action.Reducible {
    type: Action.Type.IgnoreTagSuccess,
    tag: IgnoredTag
}

export const ignoreTagSuccessful = (tag: IgnoredTag): IgnoreTagSuccessfulAction => ({
    type: Action.Type.IgnoreTagSuccess,
    tag
});

export interface RemoveIgnoredTagSuccessfulAction extends Action.Reducible {
    type: Action.Type.RemoveIgnoredTagSuccess,
    tag: IgnoredTag
}

export const removeIgnoredTagSuccessful = (tag: IgnoredTag): RemoveIgnoredTagSuccessfulAction => ({
    type: Action.Type.RemoveIgnoredTagSuccess,
    tag
});

export interface FetchIgnoredTagsSuccessAction extends Action.Reducible {
    type: Action.Type.FetchIgnoredTagsSuccess,
    tags: IgnoredTag[]
}

export const fetchIgnoredTagsSuccess = (tags: IgnoredTag[]): FetchIgnoredTagsSuccessAction => ({
    type: Action.Type.FetchIgnoredTagsSuccess,
    tags
});