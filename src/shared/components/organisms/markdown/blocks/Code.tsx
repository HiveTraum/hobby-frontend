import * as React from "react";
import {CODE_BLOCK} from "@blueprintjs/core/lib/cjs/common/classes";

interface Props {
    value: string;
}

const Code = ({value}: Props) => <pre className={CODE_BLOCK}>
        <code>{value}</code>
    </pre>;

export default Code;