import * as React from 'react';
import {Tab, Tabs} from "@blueprintjs/core";
import RegistrationForm from "./RegistrationForm";
import SignInForm from "./SignInForm";

const Entrance = () => {
    return <Tabs id="entrance-tabs" large>
        <Tab id="sign-in" title="Войти" panel={<SignInForm/>}/>
        <Tab id="sign-up" title="Регистрация" panel={<RegistrationForm/>}/>
    </Tabs>
};

export default Entrance;