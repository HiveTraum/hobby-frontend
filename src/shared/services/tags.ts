import Tag from "../models/Tag";
import getEndpoint, {Endpoint} from "../layers/api/api";
import axios from 'axios';

export const list = async (): Promise<Tag[]> => {
    const response = await axios.get(getEndpoint(Endpoint.Tags()));
    if (response.status === 200) return response.data;
    return []
};

interface CreateParams {
    title: string,
    color: string
}

export const create = (params: CreateParams) => axios.post<Tag>(getEndpoint(Endpoint.Tags()), params);