import * as React from "react";
import Tag from "../../models/Tag";
import TagChip from "./TagChip";

interface Props {
    tags: Tag[],
    large?: boolean
}

const TagList = ({tags, large = false}: Props) => <div>
    {tags.map(tag => <TagChip key={tag.id} tag={tag} large={large}/>)}
</div>;

export default TagList;