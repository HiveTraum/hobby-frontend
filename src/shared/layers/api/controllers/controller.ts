import Model from "../../../models/Model";

interface Pagination {
    count: number,
    next: number | null,
    previous: number | null
}

export default class Controller<T extends Model, Detail = any, List = any, Options = any> {

    private readonly url: URL;
    private _filter: Partial<T> = {};
    private pagination?: Pagination;

    private currentPage?: number;

    constructor(url: string) {
        this.url = new URL(url);
    }

    set filter(value) {
        this._filter = value;
        delete this.pagination;
    }

    get filter() {
        return this._filter;
    }

    options = async (id?: number) => {

    };

    async read(): Promise<Array<T> | null> {
        let params = [];
        for (let key in this.filter) {
            if (!this.filter.hasOwnProperty(key)) continue;

            params.push(`${key}=${this.filter[key]}`)
        }

        if (this.currentPage) params.push(`page=${this.currentPage}`);

        let response = await fetch(`${this.url}?${params.join("&")}`, {
            credentials: 'include'
        });

        let json = await response.json();

        delete this.currentPage;
        if (json.results && typeof json.count === 'number' && typeof json.next === 'number' && typeof json.previous === 'number') {
            this.pagination = json;
        }

        if (Array.isArray(json.results)) json = json.results;

        return json as T[];
    }

    hasNext(): boolean {
        return this.pagination !== undefined && this.pagination.next !== null;
    }

    hasPrevious(): boolean {
        return this.pagination !== undefined && this.pagination.previous !== null;
    }

    next() {
        if (this.pagination !== undefined && this.pagination.next !== null) this.currentPage = this.pagination.next;
    }

    previous() {
        if (this.pagination !== undefined && this.pagination.previous !== null) this.currentPage = this.pagination.previous;
    }

    get isPaginated(): boolean {
        return this.hasNext() || this.hasPrevious();
    }

    //
    // async write(objects: T): number {
    //     let request = new Request(this.url.toString());
    //     let response = await fetch(this.url, {
    //         method: 'post',
    //         body: JSON.stringify(objects),
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json'
    //         },
    //         credentials: 'same-origin'
    //     });
    //
    //     return response.
    // }
    //
    // update(objects) {
    // }
    //
    // remove(objects) {
    // }
}