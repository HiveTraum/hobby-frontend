import * as React from 'react';
import {ReactNode} from "react";


interface Props {
    children?: ReactNode
}

const List = (props: Props) => {
    return <ul className="list-group">{props.children}</ul>
};

export default List;