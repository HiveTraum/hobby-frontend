import {ReactNode} from "react";
import React = require("react");

export enum AlignmentPosition {
    Start = "start",
    Center = "center",
    End = "end"
}

interface Props {
    vertical?: AlignmentPosition,
    horizontal?: AlignmentPosition,
    children: ReactNode
}

const Alignment = (props: Props) => {

    const className = ['row'];

    if (props.horizontal) className.push(`justify-content-${props.horizontal}`);
    if (props.vertical) className.push(`align-self-${props.vertical}`);

    return <div className="container">
        <div className={className.join(" ")}>{props.children}</div>
    </div>
};

export default Alignment;