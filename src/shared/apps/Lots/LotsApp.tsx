import * as React from "react";
import {FormEvent} from "react";
import {Button, InputGroup, Intent, NonIdealState, Spinner} from "@blueprintjs/core";
import * as lotService from '../../services/lots';
import Lot from "../../models/Lot";
import {debounce} from "../../layers/runtime";
import LotList from "./components/LotList";
import {Link} from "react-router-dom";
import {references} from "../../routes";
import TagSelectList from "../Tags/TagSelectList";
import {connect} from "react-redux";
import RootState from "../../state/RootState";
import {fetchIgnoredTags} from "../../state/actions/ignoredTags";
import Action from "../../state/actions";

interface State {
    isSearching: boolean,
    searchableValue: string,
    lots: Lot[],
    pages: number,
    page: number
}

interface DispatchProps {
    fetchIgnoredTags: () => void;
}

class LotsAppComponent extends React.Component<DispatchProps, State> {

    state: State = {
        isSearching: false,
        searchableValue: "",
        lots: [],
        pages: 0,
        page: 0
    };

    handleSearchChange = (e: FormEvent<HTMLInputElement>) => this.setState({searchableValue: (e.target as HTMLInputElement).value}, this.requestToFetchLots);

    onDocumentVisible = () => {
        if (document.hidden) return;
        this.fetchData();
    };

    fetchData = () => {
        this.requestToFetchLots();
        this.fetchIgnoredTags();
    };

    componentDidMount() {
        this.fetchData();
        if (typeof document !== "undefined") document.addEventListener("visibilitychange", this.onDocumentVisible);
    }

    componentWillUnmount() {
        if (typeof document !== "undefined") document.removeEventListener("visibilitychange", this.onDocumentVisible);
    }

    fetchIgnoredTags = () => this.props.fetchIgnoredTags();

    fetchLots = debounce(async () => {
        const response = await lotService.list({search: this.state.searchableValue, ignored: true});
        this.setState({
            lots: response.results,
            isSearching: false
        })
    }, 300);

    requestToFetchLots = () => this.setState({
        isSearching: true
    }, this.fetchLots);

    render() {

        const {isSearching, searchableValue, lots} = this.state;

        const searchInput = <InputGroup large leftIcon="search" onChange={this.handleSearchChange}
                                        value={searchableValue} placeholder="Поиск...."
                                        rightElement={isSearching ? <Spinner size={15}/> :
                                            <Button large icon="arrow-right" minimal
                                                    onClick={this.requestToFetchLots}/>}/>;

        return <div className="container">
            <h1>Лоты</h1>

            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-8 col-lg-10 mt-3">{searchInput}</div>
                <div className="col-xs-12 col-sm-12 col-md-4 col-lg-2 mt-3">
                    <Link to={references.createLot()}>
                        <Button large fill icon="plus" intent={Intent.SUCCESS}>Добавить</Button>
                    </Link>
                </div>
                <div className="col-12">
                    <TagSelectList onClick={this.requestToFetchLots}/>
                </div>
            </div>

            <div>
                {lots.length >= 1
                    ? <LotList lots={lots}/>
                    : <div style={{height: 400}}>
                        <NonIdealState
                            icon="search"
                            title="По данному запросу ничего не найдено"
                            description="Попробуйте изменить параметры запроса или зайдите чуть попозже,
                         мы обязательно придумаем как потратить ваш свободный вечер!"
                            action={searchInput}/>
                    </div>}
            </div>
        </div>
    }
}

const LotsApp = connect<{}, DispatchProps, {}, RootState>(
    undefined,
    (dispatch: Action.DelayedDispatch) => ({
        fetchIgnoredTags: () => dispatch(fetchIgnoredTags())
    })
)(LotsAppComponent);

export default LotsApp;