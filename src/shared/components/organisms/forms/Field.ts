enum FieldType {
    String = "string"
}

interface FieldConstructorParams {
    type?: FieldType,
    required?: boolean,

    label?: string,
    placeholder?: string,
    help?: string
}

export default class Field<T = any> {
    name: keyof T;
    params: FieldConstructorParams;

    constructor(name: keyof T, {type = FieldType.String, required = false, label, placeholder, help}: FieldConstructorParams = {}) {
        this.name = name;
        this.params = {type, required, label, placeholder, help};
    }
}