import * as lotsService from "./lots";
import * as userService from "./users";
import * as tagsService from './tags';
import * as betsService from './bets';
import * as ignoredTagsService from './ignoredTags';
import * as authenticationService from './authentication';

const services = {
    lots: lotsService,
    users: userService,
    tags: tagsService,
    bets: betsService,
    ignoredTags: ignoredTagsService,
    auth: authenticationService
};

export default services;